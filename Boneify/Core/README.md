# Boneify Loyalty App for Magento 2

## Overview

The Boneify Integration App for Magento 2 seamlessly integrates your Magento 2 store with the Boneify platform, providing enhanced customer experiences by rendering the Boneify user dashboard on the profile page. Customers can effortlessly collect Boneify reward points and redeem them during the order placement process, fostering loyalty and engagement.

## Features

- **User Dashboard Integration:** Render the Boneify user dashboard on the customer profile page, providing a unified experience.

- **Reward Points Collection:** Automatically collect Boneify reward points based on customer purchase history, encouraging loyalty.

- **Order Integration:** Sync customer orders with Boneify, attaching reward points to each order for accurate tracking.

- **Redemption Process:** Allow customers to redeem accumulated reward points during the checkout process.

- **Customization Options:** Configure integration settings to customize the display and reward points earning/redemption.

- **Error Handling and Logging:** Comprehensive error handling and logging for a reliable integration process.

## Installation

1. **Composer Installation:**
   ```bash
   composer require boneify/core
   composer require boneify/loyalty
   ```

   
## Manual Installation:
1. Download the ZIP file from GitHub.
2. Extract contents into app/code/Boneify/
## Module Activation:
``` 
bin/magento module:enable Boneify_Core Boneify_Loyalty
bin/magento setup:upgrade
bin/magento setup:di:compile
```
## Configuration:
```Navigate to Stores > Configuration > Boneify Integration to configure API credentials and settings.```

## Usage
1. API Configuration:
Obtain API credentials from Boneify.
Enter credentials in Magento 2 configuration.

2. Dashboard Rendering:
Configure settings to render Boneify user dashboard on the customer profile page.
Reward Points Management:
Enable reward points collection based on customer purchases. Configure redemption options during checkout.

3. Cron Setup:
Ensure Magento 2 cron is set up for automated data synchronization.

Support and Documentation
For questions, issues, or additional information, refer to the official documentation.

Contact our support team at support@boneify.com.
Contributing

We welcome contributions and feedback.
 
Thank you for choosing the Boneify Loyalty App for Magento 2!