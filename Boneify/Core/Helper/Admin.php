<?php

namespace Boneify\boneifyadmin\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Registry;

class Admin extends AbstractHelper {

    protected $_objectManager = '';
    protected $_projectId = '';
	protected $_secretKey = '';
    protected $_scopeConfig = '';
    protected $_loyaltyEmbedBlockObj= '';
    protected $_registry = '';
    protected $_productRepository = '';

    public function __construct(Registry $registry, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, array $data = []) {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_scopeConfig = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
        $this->_projectId = $this->_scopeConfig->getValue('boneifyadmin/boneifygeneral/projectId');
		$this->_secretKey= $this->_scopeConfig->getValue('boneifyadmin/boneifygeneral/secretKey');
        $this->_registry = $registry;
        $this->_productRepository = $productRepository;
        //create Eagle Cloud loyaltyembed block class object
        $this->_loyaltyEmbedBlockObj = $this->_objectManager->get('Boneify\boneifyadmin\Block\loyaltyembed');
    }
    /*
     * load Eagle Cloud loyaltyembed
     * Function defination written on loyaltyembed block file
     * File Path : Boneify/boneifyadmin/Block/loyaltyembed.php
     */

    public function loadLoyaltyembed($data = array()) {
        return $this->_loyaltyEmbedBlockObj->loadloyaltyembed(array("projectId" => $this->_projectId,"secretKey"=>$this->_secretKey));
    }

    /*
     * get logged user information
     *       
     */

    public function getCustomerValue() {
        $firstname = '';
        $lastname = '';
        $email = '';
        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');
        if ($customerSession->isLoggedIn()) {
            $customer = $customerSession->getCustomer();
            $firstname = $customer->getFirstname();
            $lastname = $customer->getLastname();
            $email = $customer->getEmail();
        }
        $userinfo = array("firstName" => $firstname, "lastName" => $lastname, "email" => $email);
        return $userinfo;
    }
}
