<?php
namespace Boneify\Core\Block;
use \Magento\Framework\View\Element\Template;
class loadloyaltyembed extends Template
{
    
    public function __construct()
    {
        
    }
    public function loadloyaltyembed($data = array())
    {
        $script ='<!-- Boneify JS -->'.PHP_EOL;
        $script.='<script type="text/javascript">'.PHP_EOL;
        $script.='window.BoneifyLoyaltyAsyncInit = function(){'.PHP_EOL;
        $script.='BoneifyLoyaltyObj.startinit({'.PHP_EOL;
        $script.='projectID:'.$data['projectId'].',JwtToken:'.$data['token'].',loyaltyUserId:'.$data['loyaltyId'].',loyaltydashboarddiv:"Boneify_loyalty",iframeHeight:"100%",iframewidth:"100%"})};'.PHP_EOL;
        $script.='(function(d){'.PHP_EOL;
        $script.='var myBoneifyjs, BoneifyId =Boneify_loyalty, myBoneifyref = d.getElementsByTagName(script)[0];'.PHP_EOL;
        $script.='if (d.getElementById(BoneifyId)) {return;}'.PHP_EOL;
        $script.='myBoneifyjs = d.createElement(script); myBoneifyjs.BoneifyId = BoneifyId; myBoneifyjs.async = true;'.PHP_EOL;
        $script.='myBoneifyjs.src = "https://cdn.Boneify.com/v1/js/loyalty-embed.js";'.PHP_EOL;
        $script.='myBoneifyref.parentNode.insertBefore(myBoneifyjs, myBoneifyref);'.PHP_EOL;
        $script.='}(document));';
        $script.='</script> '.PHP_EOL;
        return $script;
    }
    
}

