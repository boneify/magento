<?php
namespace Boneify\Core\Model\Config;
 
class Custom implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'customer_id', 'label' => __('Customer ID')],
            ['value' => 'email', 'label' => __('Email')]
        ];
    }
}