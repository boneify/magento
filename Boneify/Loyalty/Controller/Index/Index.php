<?php

namespace Boneify\Loyalty\Controller\Index;

use \Magento\Framework\App\Action\Action;

class Index extends Action
{
    protected $resultPageFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$customerSession = $objectManager->create('Magento\Customer\Model\Session');

		if ($customerSession->isLoggedIn()) {
			return $this->resultPageFactory->create();
		} else {
			$resultRedirect = $this->resultRedirectFactory->create();
			$resultRedirect->setPath('customer/account/login');
			return $resultRedirect;
		}
    }
}

