<?php

namespace Boneify\Loyalty\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Boneify\Loyalty\Helper\JWT;
use Throwable;

class LoyaltyService implements ObserverInterface
{

    protected $_objectManager;
    protected $_objHelper;
    protected $_projectId;
    protected $_secretKey;
    protected $_baseURL;
	protected $_curl;
    protected $_jsonHelper;
    protected $_jwtHelper;
	protected $_curlFactory;
	protected $_logger;
	protected $_scopeConfig;

	public function __construct(
         JWT $jwtHelper,
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\HTTP\Client\Curl $curl,
        ScopeConfigInterface $scopeConfig
    ) {	
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_objHelper = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
        $this->_projectId = $this->_objHelper->getValue('boneifyadmin/boneifygeneral/projectId');
        $this->_secretKey = $this->_objHelper->getValue('boneifyadmin/loyalty/secretKey');
        $this->_baseURL = $this->_objHelper->getValue('boneifyadmin/loyalty/loyaltyApiURL');
		$this->_scopeConfig = $scopeConfig;
        $this->_jwtHelper = $jwtHelper;
		$this->_curl = $curl;
        $this->_curlFactory = $curlFactory;
        $this->_jsonHelper = $jsonHelper;
		$this->_logger = $logger;
     }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		if ($this->_scopeConfig->isSetFlag(
            'boneifyadmin/loyalty/enableLoyalty',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )) {
				$eventListener = $observer->getEvent()->getName();
				if ( $eventListener == 'customer_register_success' ) {
				$this->customerRegisterSuccess($observer);
				}
				if ($eventListener == 'newsletter_subscriber_save_after') {
				$this->newsletterSubscriberSaveAfter($observer);
				}
				if ($eventListener == 'sales_order_save_after') {
					$this->orderCaptured($observer);
				}
				if ($eventListener == 'sales_order_payment_refund') {
				$this->orderReturn($observer);
				}

				if ($eventListener == 'order_cancel_after') {
				$this->orderCancel($observer);
				}

				if ($eventListener == 'sales_order_shipment_save_after') {
				$this->orderShipmentAfter($observer);
				}
			}
    }

    public function customerRegisterSuccess($observer)
    {
        $eventListener = $observer->getEvent();
        $customer = $eventListener->getCustomer();
        $userEmail = $customer->getEmail(); // user Email
        $loyaltyFname = $customer->getFirstname(); // First Name
        $loyaltyLname = $customer->getLastname(); // Last Name
        $customerId = $customer->getId();

        if ($userEmail) {
            $loyaltyBaseURL = $this->_baseURL;
            $projectId = $this->_projectId;
            $signupActionId = $this->_objHelper->getValue('boneifyadmin/loyalty/CreateAccount');
            //Create user
            $requestURL = $loyaltyBaseURL . "/v1/customer";
            $data = array("customer_id" => $userEmail,
                            "email" => $userEmail,
                            'first_name' => $loyaltyFname, 
                            'last_name' => $loyaltyLname,
                            'status' => "ACTIVE",
                            'source' => "Register Page",
                            'optin_status'=>"YES",
                            'loyalty_join_date'=>date("Y-m-d"),
                            'added_date'=>date("Y-m-d")  							
                        );
            $token = $this->getJwtToken($data);
			//$this->_logger->info("token=>".$token); 
           $loyaltyResponse= $this->loyaltyPostServiceAPI($requestURL, $data, $token);
		   $responseUser=$this->_jsonHelper->jsonDecode($loyaltyResponse);
			//Give points
			if(isset($responseUser['customer_id']))
			{
			$pointRequestUrl = $loyaltyBaseURL . "/v1/point";
			$postData =array('customer_id' => $userEmail, 'event_id' => $signupActionId, 'point_type' => "credit");
			$token = $this->getJwtToken($postData);
			$response = $this->loyaltyPostServiceAPI($pointRequestUrl, $postData, $token);
			}
        }
    }

	public function newsletterSubscriberSaveAfter($observer)
    {
		$subscription = $observer->getEvent()->getSubscriber();
        $customerId= $subscription->getEmail(); // user Email
        if($customerId) {

            $loyaltyBaseURL = $this->_baseURL;
            $projectId = $this->_projectId;
            $signupNewsletterEvenId = $this->_objHelper->getValue('boneifyadmin/loyalty/SignupNewsletter');
            //Give points
            $pointRequestUrl = $loyaltyBaseURL . "/v1/point";
            $postData = array('customer_id' => $customerId, 'event_id' => $signupNewsletterEvenId, 'point_type' => "credit");
            $token = $this->getJwtToken($postData);
            $response = $this->loyaltyPostServiceAPI($pointRequestUrl, $postData, $token);
        }

    }
	 public function orderCaptured($observer)
    {
        /** @var $order \Magento\Sales\Model\Order */
        $order = $observer->getEvent()->getOrder();
        $loyaltyBaseURL = $this->_baseURL;   
        $orderID    = $order->getIncrementId();
        $saleAmount = $order->getGrandTotal();
        $emailId    = $order->getCustomerEmail();
        $couponCode = $order->getCouponCode();
        $items = $order->getAllItems();

        $boneifyIdentifier = $this->_scopeConfig->getValue(
            'boneifyadmin/loyalty/BoneifyIdentifier',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($boneifyIdentifier == 'email') {
            $customerId = $emailId;
        } else {
            $customerId = $order->getCustomerId();
        }   
        if ($orderID !="" && $customerId !='') {
            $i = 0;
            $productDetails = [];
            $itemsDiscountAmount = 0;
            foreach ($items as $item) {
                if (!$item->isDeleted() && !$item->getParentItem()) {

                    $productid = $item->getProductId();
                    $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($productid);
                    $categoriesIds = $product->getCategoryIds();
                    $categoryNames = [];
                    $categoryNamesData = "";

                    if (!empty($categoriesIds)) {
                        foreach ($categoriesIds as $categoryId) {
                            $cat = $this->_objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);
                            $catName = $cat->getName();
                            $categoryNames[] = $cat->getName();
                        }

                        $categoryIds = implode(',', $categoriesIds);
                        $categoryNamesData = implode(',', $categoryNames);
                    }

                    $productDetails[$i] = [
                        'sku_id' => $item->getSku(),
                        'unit_price' => $item->getPrice(),
                        'quantity' => $item->getQtyOrdered(),
                        'category_id' => $categoryIds,
                        'category_name' => $categoryNamesData
                    ];
                    $i++;
                }
            }

            $orderData = [
                'order_id' => $orderID, 'customer_id' => $customerId, 'email' => $emailId, 'total_price' => $saleAmount,
                'source' => 'web', 'coupon' => $couponCode,
                'order_items' => $productDetails
            ];
            $orderRequestUrl =$loyaltyBaseURL . "/v1/order";
			$token = $this->getJwtToken($orderData);
			$response = $this->loyaltyPostServiceAPI($orderRequestUrl, $orderData, $token);
			$this->_logger->info("response=>".$response);
			return $response;
        }
    }

    public function orderReturn($observer)
    {
		$loyaltyBaseURL = $this->_baseURL;
        $creditMemo = $observer->getEvent()->getCreditmemo();
        $order      = $creditMemo->getOrder();
        $orderId = $order->getIncrementId();
        $items = $creditMemo->getItems();
        $productDetails = [];
        $i = 0;

        foreach ($items as $item) {
            if ($item->getQty() > 0) {
                    $productDetails[$i] = ['sku_id' => $item->getSku(), 'unit_price' => $item->getPrice(),
                    'quantity' => $item->getQty()];
                    $i++;
            }
        }
        $requestURL = $loyaltyBaseURL . "/v1/order/".$orderId;
        $returnData = ['order_id' => $orderId, 'status' => 'return', 'order_items' => $orderDetails];
        
        $token = $this->getJwtToken($returnData);
        $response = $this->loyaltyPostServiceAPI($requestURL, $returnData, $token);
        $returnOrderResponse= $this->jsonHelper->jsonDecode($response); 
        return $returnOrderResponse;
    }

    public function orderCancel($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getIncrementId();
		$loyaltyBaseURL = $this->_baseURL;
        $requestURL = $loyaltyBaseURL . "//v1/order/".$orderId;
        if (!empty($orderDetails)) {
            $orderCancelData = ['order_id' => $orderId, 'status' => 'cancel', 'order_items' => $orderDetails];
        } else {
            $orderCancelData = ['order_id' => $orderId, 'status' => 'cancel'];
        }

        $token = $this->getJwtToken($orderCancelData);
        $response = $this->loyaltyPostServiceAPI($requestURL, $orderCancelData, $token);
        return $response;
    }

    public function orderShipmentAfter($observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $orderId = $order->getIncrementId();
        $items = $shipment->getItems();
        $productDetails = [];
        $i = 0;
        foreach ($items as $item) {
            if ($item->getQty() > 0) {
                    $productDetails[$i] = ['sku_id' => $item->getSku(), 'unit_price' => $item->getPrice(),
                    'quantity' => $item->getQty()];
                    $i++;
            }
        }
        $loyaltyBaseURL = $this->_baseURL;
        $requestURL = $loyaltyBaseURL . "//v1/orders/".$orderId;
        $orderShipmentData = ['order_id' => $orderId, 'status' => 'ship', 'order_items' => $productDetails];
        $token = $this->getJwtToken($orderShipmentData);
        return $this->loyaltyPostServiceAPI($requestURL, $orderShipmentData, $token);
    }
   /*
    * POST CURL API
    */
    public function loyaltyPostServiceAPI($loyaltyDomainUrl, $data, $token)
	{
        $requestbody = $this->_jsonHelper->jsonEncode($data);
		//$this->_logger->info("projectId=>".$this->_projectId); 
        $headers =["Content-Type" => "application/json","X-Auth-Token" =>$token,"X-Auth-Project-Id" => $this->_projectId];
        $this->_curl->setOption(CURLOPT_SSL_VERIFYHOST, false);
        $this->_curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $this->_curl->setHeaders($headers);
        $this->_curl->post($loyaltyDomainUrl, $requestbody);
        $response = $this->_curl->getBody();
        return $response;
    }
    private function getJwtToken($payload)
    {
        $key = $this->_secretKey;
		$jsonEncoded = $this->_jsonHelper->jsonEncode($payload);
        $encodedPayload= base64_encode($jsonEncoded);
        $codeVerifier = base64_encode(hash_hmac('sha256', $encodedPayload, $key, true));
        $signature = array(
            "exp" => time() +3600,
            "projectId" => $this->_projectId,
            "codeVerifier" => $codeVerifier
        );
        $token = $this->_jwtHelper->encode($signature, $key);
        return $token;
    }
	
}

