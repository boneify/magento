<?php
namespace Boneify\Loyalty\Helper;
header('X-Frame-Options: GOFORIT');
use \Magento\Framework\App\Helper\AbstractHelper;
use \Boneify\Loyalty\Helper\JWT;

class LoyaltyHelper extends AbstractHelper
{
    protected $_objHelper;
    protected $_objectManager;
    protected $_projectId;
    protected $_secretKey;
    protected $_baseURL;
    protected $_helper;
	protected $_curl;
    protected $_jwtHelper;
    protected $_jsonHelper;
    protected $_boneifyIdentifier;
    protected $_storeManager;
	protected $_logger;

    public function __construct(
    JWT $jwtHelper, 
    \Magento\Framework\Json\Helper\Data $jsonHelper,
    \Magento\Framework\HTTP\Client\Curl $curl,
    \Magento\Store\Model\StoreManagerInterface $storeManager ,
    \Psr\Log\LoggerInterface $logger)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_objHelper = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
        $this->_projectId = $this->_objHelper->getValue('boneifyadmin/boneifygeneral/projectId');
        $this->_secretKey = $this->_objHelper->getValue('boneifyadmin/loyalty/secretKey');
        $this->_baseURL = $this->_objHelper->getValue('boneifyadmin/loyalty/loyaltyApiURL');
		$this->_boneifyIdentifier = $this->_objHelper->getValue('boneifyadmin/loyalty/BoneifyIdentifier');
        $this->_jwtHelper = $jwtHelper; 
        $this->_jsonHelper = $jsonHelper;
        $this->_curl = $curl;
        $this->_storeManager = $storeManager;
		$this->_logger = $logger;
    }

    private function getJwtToken($data)
    {
        $key = $this->_secretKey;
        $encodedPayload= base64_encode($data);
        $hmac = base64_encode(hash_hmac('sha256', $encodedPayload, $key, true));

        $signature = array(
            "exp" => time() +3600,
            "projectId" => $this->_projectId,
            "codeVerifier" => $hmac
        );

        $token = $this->_jwtHelper->encode($signature, $key);
        return $token;
    }

    protected function loadloyaltyembed($customerId)
    {
        $projectId = $this->_projectId;
		
        $token = $this->getJwtToken($customerId);
		
        $script ='<!-- Boneify JS -->'.PHP_EOL;
        $script.='<script type="text/javascript">'.PHP_EOL;
        $script.='window.boneifyLoyaltyAsyncInit = function(){'.PHP_EOL;
        $script.='boneifyLoyaltyObj.startinit({'.PHP_EOL;
        $script.='projectID:'.$projectId.',JwtToken:"'.$token.'",loyaltyUserId:"'.$customerId.'",loyaltydashboarddiv:"Boneify_loyalty",iframeHeight:"100%",iframewidth:"100%"})};'.PHP_EOL;
        $script.='(function(d){'.PHP_EOL;
        $script.='var myBoneifyjs, BoneifyId ="Boneify_loyalty", myBoneifyref = d.getElementsByTagName("script")[0];'.PHP_EOL;
        $script.='if (d.getElementById(BoneifyId)) {return;}'.PHP_EOL;
        $script.='myBoneifyjs = d.createElement("script"); myBoneifyjs.BoneifyId = BoneifyId;  myBoneifyjs.type ="text/javascript"; myBoneifyjs.async = true;'.PHP_EOL;
        $script.='myBoneifyjs.src = "https://cdn.boneify.com/v1/js/loyalty-embed.js";'.PHP_EOL;
        $script.='myBoneifyref.parentNode.insertBefore(myBoneifyjs, myBoneifyref);'.PHP_EOL;
        $script.='}(document));';
        $script.='</script> '.PHP_EOL;
		$script.= "<div id='Boneify_loyalty'></div>" . PHP_EOL;
        return $script;
    }

    //Loyalty Dashboard
    public function loadLoyaltyDashboard()
    {
        $customerSession = $this->_objectManager->create('Magento\Customer\Model\Session');
        if ($customerSession->isLoggedIn()) {
                $customerId = $customerSession->getCustomerId();  // get Customer Id
                $email = $customerSession->getCustomer()->getEmail();
                $this->_boneifyIdentifier;
            if ($this->_boneifyIdentifier == 'email') {
                $getloadloyaltyembed = $this->loadloyaltyembed($email);
            } else {
                $getloadloyaltyembed = $this->loadloyaltyembed($customerId);
            }
            return $getloadloyaltyembed;
        }
    }

    public function createUserService($createCustomerArr=array())
    {
        if ($createCustomerArr['customerId'] != '') {
            $loyaltyBaseURL = $this->_baseURL;
            $requestURL = $loyaltyBaseURL . "/v1/customer";
            $otherAttributes['extendedAttribute']['source'] = 'magento';
            $data = $otherAttributes;
            $data['id'] = $createCustomerArr['customerId'];
            $data['email'] = $createCustomerArr['email'];
            $data['firstName'] =$createCustomerArr['firstName'];
            $data['lastName'] = $createCustomerArr['lastName'];
            $data['status'] = 'ACTIVE';
            $token = $this->getJwtToken($data);
            $response = $this->loyaltyPostServiceAPI($requestURL, $data, $token);
            return $response;
        }
    }

    public function updateUserService($customerId,$updateCustomerArr=array()) {
            if ($customerId != '') {
                $loyaltyBaseURL = $this->_baseURL;
                $requestURL = $loyaltyBaseURL . "/v1/customer/".$customerId;
                $data = $otherAttributes;
                $data['id'] =$updateCustomerArr['id'];
                $data['email'] = $updateCustomerArr['email'];
                
                $token = $this->getJwtToken($data);
                $response = $this->loyaltyPostServiceAPI($requestURL, $data, $token);
                return $response;
            }
    }

    public function givePointsAdminService($customerId, $actionName)
    {

        if ($customerId!='' && $actionName!='') 
        {
            $loyaltyBaseURL = $this->_baseURL;
            
			$eventId = $this->_objHelper->getValue('boneifyadmin/loyalty/'.$actionName);
			if($eventId!='')
            {
				$requestUrl = $loyaltyBaseURL . "/v1/points";
				$postData = array('customer_id' => $customerId, 'event_id' => $eventId, 'point_type' => "credit");
                // print_r($postData);die;
                $token = $this->getJwtToken($postData);
				$response = $this->loyaltyPostServiceAPI($requestUrl, $postData, $token);
				return $response;
			}
        }
    }

	public function givePointsCustomService($customerPointArr=array())
    {
			$customerId=isset($customerPointArr['customerId']) ? $customerPointArr['customerId']:"";
			$eventId=isset($customerPointArr['eventId']) ? $customerPointArr['eventId']:"";
			$creditType=isset($customerPointArr['creditType']) ? $customerPointArr['creditType']:"";
			$points=isset($customerPointArr['points']) ? $customerPointArr['points']:0;
			$reason=isset($customerPointArr['reason']) ? $customerPointArr['reason']:null;
			$orderId=isset($customerPointArr['orderId']) ? $customerPointArr['orderId']:null;
			if( $customerId !='' && $eventId !='') {
            $loyaltyBaseURL = $this->_baseURL;

            //Credit/Debit points
            $requestUrl = $loyaltyBaseURL . "/v1/points";
			$pointPayload= array('customer_id' => $customerId, 'event_id' => $eventId, 'point_type' => $creditType, 'reason' => $reason);
            if($creditType == 'DEBIT')
            {
                $pointPayload['debit'] = $points;
                $pointPayload['orderId'] = $orderId;
            }
            else
            {
                $pointPayload['credit'] = $points;
            }

            $token = $this->getJwtToken($pointPayload);
            $response = $this->loyaltyPostServiceAPI($requestUrl, $pointPayload, $token);
            return $response;
        }
    }

    public function eligibleRewardsService($customerId)
    {

        if ($customerId!='') {
            $loyaltyBaseURL = $this->_baseURL;
            //User details
            $requestURL = $loyaltyBaseURL . "/v1/reward/" . $customerId;
            $token = $this->getJwtToken($customerId);
            $response = $this->loyaltyGetServiceAPI($requestURL,$token);
            
            return $response;
        }
    }

    public function redeemRewardService($rewardRewardArr=array())
    {

			$customerId=isset($rewardRewardArr['customerId']) ? $rewardRewardArr['customerId']:"";
			$rewardId=isset($rewardRewardArr['rewardId']) ? $rewardRewardArr['rewardId']:"";
        if ( $customerId!='' && $rewardId!='' ) {
            $loyaltyBaseURL = $this->_baseURL;
            //User details
            $requestUrl = $loyaltyBaseURL . "/v1/claimedreward";
            $postData = array('customer_id' => $customerId, 'reward_id' => $rewardId);
            $token = $this->getJwtToken($postData);
            $response = $this->loyaltyPostServiceAPI($requestUrl, $postData, $token);
            return $response;
        }
    }

    public function userDetailService($customerId)
    {
		
        if ($customerId!='') {
            $loyaltyBaseURL = $this->_baseURL;
            //User details
            $requestURL = $loyaltyBaseURL . "/v1/customer/" . $customerId;
            $token = $this->getJwtToken($customerId);
            $response = $this->loyaltyGetServiceAPI($requestURL,$token);
			return $response;
        }
    }

    public function userPointsDetailService($customerId)
    {
        
        if ($customerId!='') {
            $loyaltyBaseURL = $this->_baseURL;
            //User details
            $requestURL = $loyaltyBaseURL . "/v1/points/" . $customerId;
            $token = $this->getJwtToken($customerId);
            $response = $this->loyaltyGetServiceAPI($requestURL,$token);
            return $response;
        }
    }

    public function tierDetailService($customerId)
    {
        
        if ($customerId!='') {
            $loyaltyBaseURL = $this->_baseURL;
            //Tier details
            $requestURL = $loyaltyBaseURL . "/v1/customer/" . $customerId. "/tiers";
            $token = $this->getJwtToken($customerId);
            $response = $this->loyaltyGetServiceAPI($requestURL,$token);
            return $response;
        }
    }

    /*
    * POST CURL API
    */
    public function loyaltyPostServiceAPI($loyaltyDomainUrl, $data, $token)
	{
        $requestbody = $this->_jsonHelper->jsonEncode($data);
        $headers =["Content-Type" => "application/json","X-Auth-Token" =>$token,"X-Auth-Project-Id" => $this->_projectId];
        $this->_curl->setOption(CURLOPT_SSL_VERIFYHOST, false);
        $this->_curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $this->_curl->setHeaders($headers);
        $this->_curl->post($loyaltyDomainUrl, $requestbody);
        $response = $this->_curl->getBody();
		 //$this->_logger->info("loyaltyPostServiceAPI=>".$response);
		 //$this->_logger->info("loyaltyDomainUrl=>".$loyaltyDomainUrl);
        return $response;
    }
	public function loyaltyGetServiceAPI($loyaltyDomainUrl, $token)
    {
        $headers =["Content-Type" => "application/json","X-Auth-Token" =>$token,"X-Auth-Project-Id" => $this->_projectId];
        $this->_curl->setOption(CURLOPT_SSL_VERIFYHOST, false);
        $this->_curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $this->_curl->setHeaders($headers);
        $this->_curl->get($loyaltyDomainUrl);
        $response = $this->_curl->getBody();
        return $response;
    }
}

